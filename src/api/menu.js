/*
 * @Author: Raiz
 * @since: 2020-07-31 15:15:00
 * @lastTime: 2020-07-31 15:16:36
 * @LastEditors: Raiz
 * @Description:
 */
import request from '@/utils/request'

const baseUrl = '/menuModule'
export function getMenuModule(data) {
  return request({
    url: baseUrl + '/queryMenuModule',
    method: 'post',
    data
  })
}

