/*
 * @Author: Raiz
 * @since: 2020-07-07 14:02:44
 * @lastTime: 2020-07-31 14:06:53
 * @LastEditors: Raiz
 * @Description:
 */
import request from '@/utils/request'

const baseUrl = '/user'
export function login(data) {
  return request({
    url: '/oauth/token',
    method: 'post',
    data
  })
}

export function getInfo(data) {
  return request({
    url: baseUrl + '/getUser',
    method: 'post',
    data
  })
}

export function logout(data) {
  return request({
    url: baseUrl + '/logOut',
    method: 'post',
    data
  })
}

export function getAllUser(data) {
  return request({
    url: baseUrl + '/getAllUser',
    method: 'post',
    data
  })
}

export function disableUser(data) {
  return request({
    url: baseUrl + '/disableUser',
    method: 'post',
    data
  })
}

export function updateUser(data) {
  return request({
    url: baseUrl + '/updateUser',
    method: 'post',
    data
  })
}

export function getUserById(data) {
  return request({
    url: baseUrl + '/getUserById',
    method: 'post',
    data
  })
}
