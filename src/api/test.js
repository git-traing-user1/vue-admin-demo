/*
 * @Author: Raiz
 * @since: 2020-07-27 09:07:01
 * @lastTime: 2020-07-29 20:55:28
 * @LastEditors: Raiz
 * @Description: 123
 */

import request from '@/utils/request'
const baseUrl = 'user/'
export function addOp() {
  return request({
    url: baseUrl + 'add',
    method: 'post'
  })
}

export function deleteOp() {
  return request({
    url: baseUrl + 'delete',
    method: 'post'
  })
}
export function updateOp() {
  return request({
    url: baseUrl + 'update',
    method: 'post'
  })
}

export function queryOp() {
  return request({
    url: baseUrl + 'query',
    method: 'post'
  })
}

