/*
 * @Author: Raiz
 * @since: 2020-07-07 14:02:48
 * @lastTime: 2020-07-31 11:07:48
 * @LastEditors: Raiz
 * @Description:
 */
import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const TokenType = 'Token-Type'
const RefreshToken = 'Refresh-Token'
const Routes = 'Routes'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getTokenType() {
  return Cookies.get(TokenType)
}

export function setTokenType(tokenType) {
  return Cookies.set(TokenType, tokenType)
}

export function removeTokenType() {
  return Cookies.remove(TokenType)
}

export function getRefreshToken() {
  return Cookies.get(RefreshToken)
}

export function setRefreshToken(refreshToken) {
  return Cookies.set(RefreshToken, refreshToken)
}

export function removeRefreshToken() {
  return Cookies.remove(RefreshToken)
}

export function getRoutes() {
  return Cookies.get(Routes)
}

export function setRoutes(routes) {
  return Cookies.set(Routes, routes)
}

export function removeRoutes() {
  return Cookies.remove(Routes)
}

