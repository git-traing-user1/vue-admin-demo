/*
 * @Author: Raiz
 * @since: 2020-07-07 14:02:48
 * @lastTime: 2020-07-27 13:43:50
 * @LastEditors: Raiz
 * @Description:
 */
import store from '@/store'

/**
 * @param {Array} value
 * @returns {Boolean}
 * @example see @/views/permission/directive.vue
 */
export default function checkPermission(value) {
  if (value && value instanceof Array && value.length > 0) {
    const permissions = store.getters && store.getters.permissions
    const permissionRoles = value

    const hasPermission = permissions.some(permission => {
      return permissionRoles.includes(permission)
    })
    return hasPermission
  } else {
    console.error(`need roles! Like v-permission="['admin','editor']"`)
    return false
  }
}
