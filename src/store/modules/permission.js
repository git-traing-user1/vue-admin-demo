/*
 * @Author: Raiz
 * @since: 2020-07-07 14:02:48
 * @lastTime: 2020-07-31 11:26:37
 * @LastEditors: Raiz
 * @Description:
 */
import { asyncRoutes, constantRoutes } from '@/router'
import { getRoutes } from '@/utils/auth'

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes() {
  const res = []
  const pathList = getRoutes()
  console.log(pathList)
  function getRoute(route) {
    const list = []
    route.forEach(ele => {
      const obj = { ...ele }
      if (obj.children) {
        const chlist = getRoute([...obj.children])
        if (chlist.length > 0) {
          obj.children = [...chlist]
          list.push(obj)
        } else if (pathList.indexOf(obj.name) > -1) {
          delete obj.children
          list.push(obj)
        }
      } else if (pathList.indexOf(obj.name) > -1) {
        delete obj.children
        list.push(obj)
      }
    })
    return [...list]
  }
  const showRoute = getRoute([...asyncRoutes])
  showRoute.forEach(element => {
    res.push(element)
  })
  // 404 page must be placed at the end !!!
  res.push({ path: '*', redirect: '/404', hidden: true })
  return [...res]
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes({ commit }, roles) {
    return new Promise(resolve => {
      console.log(roles)
      let accessedRoutes
      if (roles.includes('admin')) {
        accessedRoutes = asyncRoutes || []
      } else {
        accessedRoutes = filterAsyncRoutes(asyncRoutes, roles)
      }
      console.log(accessedRoutes)
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
